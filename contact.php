<?php
include("formHandler.php");
session_start();
if (isset($_POST['Submit']) && isset($_SESSION['form'])) {
	$_SESSION['form']->formHandler($_POST);
	if ($_SESSION['form']->validate()) {
		$_SESSION['form']->emailForm('localhost', 'info@roamersrest.co.za', '', 'michelle@roamersrest.co.za', 'Contact Form', 'Contact', 'http://www.loerieguesthouse.co.za/thanks.html');
		unset($_SESSION['form']);
	}
	else {
		$error_msg = $_SESSION['form']->get_error_msg();
		$_SESSION['form']->clearForm();
	}
}
else {
	$_SESSION['form'] = new formHandler($_POST);
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Loerie Guesthouse | Hoedspruit</title>
<link href="style.css" rel="stylesheet" type="text/css">

<!--[if lte IE 7]>
<style>
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript" src="jqueryslidemenu.js"></script>
</head>
<body>
<div id="wrap">
    <div id="header">
    <div id="menu">
    <div id="myslidemenu" class="jqueryslidemenu"><ul>
      <li><a href="index.html">Home</a></li>
      <li><a href="accommodation.html">Accommodation</a></li>
      <li><a href="rates.html">Rooms & Rates</a></li>
      <li><a href="activities.html">Activities</a></li>
      <li><a href="transfers.html">Transfers</a></li>
      <li><a href="contact.php">Contact us</a></li>
    </ul><br style="clear: left" />
    </div></div>
        <div id="booking-header"><a href="https://www.nightsbridge.co.za/bridge/book?bbid=18525" target="_blank" class="book-btn">Check availability and book online</a></div>
    </div>
    
<!-- <img src="images/frontview-2.jpg" alt="Roamers Rest" /> -->
<img src="images/D6582A5B-EAB4-4008-BE2E-DE0A2506D443.jpg" class="img-header" alt="Roamers Rest" />
    
<div id="page">
    <h1>Contact us</h1>
    <div style="float:left">
    <h2>Loerie Guesthouse</h2>
    For queries and questions, please contact us on:<br />
    Tel:	+ 27 (0)15 793 3990<br />
    Cell: 	+ 27 (0)83 292 0813<br />
    Fax:	+27 (0)15 793 2779<br />
    e-mail:	loerieguesth@lantic.net<br />
    <br /><br />
    <strong>Physical address:</strong><br />
    Loerie Guest House<br />
    85 Jakkals Street<br />
    Hoedspruit 1380 <br />
    <br />
    GPS Coordinates: -24.35296 / 30.94433<br />
    <br /><br />
    <strong>Postal address:</strong><br />
    Loerie Guest House<br />
    PO Box 1<br />
    Hoedspruit 1380<br />
    <strong>Office Hours:</strong><br /> 
    07: 00-21: 00<br />
    </div>

    <div style="clear:both">&nbsp;</div><br /><br />
    
    <form name="form" method="post" action="contact.php">
    <table width="550" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="3"><div align="center"><?php echo $error_msg; ?></div></td>
        </tr>
      <tr>
        <td width="105">Full Name:</td>
        <td width="9">&nbsp;</td>
        <td width="258"><?php
            $_SESSION['form']->input_text(array( 'name' => 'Full_name', 
                                                 'text' => 'Full name',
                                                 'type' => 'text',
												 'class' => 'long',
                                                 'required' => 'y')); ?></td>
        </tr>
      <tr>
        <td>Contact number:</td>
        <td>&nbsp;</td>
        <td><?php
            $_SESSION['form']->input_text(array( 'name' => 'Contact_number', 
                                                 'text' => 'Contact number',
                                                 'type' => 'text',
												 'class' => 'long',
                                                 'required' => 'y')); ?></td>
        </tr>
      <tr>
        <td>Email address:</td>
        <td>&nbsp;</td>
        <td><?php
            $_SESSION['form']->input_text(array( 'name' => 'Email', 
                                                 'text' => 'Email',
                                                 'type' => 'text',
												 'class' => 'long',
                                                 'required' => 'y')); ?></td>
        </tr>
      <tr>
        <td>Postal address</td>
        <td>&nbsp;</td>
        <td><?php
            $_SESSION['form']->input_text(array( 'name' => 'Postal_address', 
                                                 'text' => 'Postal address',
                                                 'type' => 'text',
												 'class' => 'long',
                                                 'required' => 'n')); ?></td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td>Comments</td>
        <td>&nbsp;</td>
        <td><?php
            $_SESSION['form']->input_textarea(array( 'name' => 'Comments', 
                                                 'text' => 'Comments',
                                                 'type' => 'text',
												 'class' => 'long',
                                                 'required' => 'y')); ?></td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><?php $_SESSION['form']->captcha_image(); ?><br /><br />
        Please enter the Anti spam Code from the image above. (Case Sensitive)<br /><Br />
        <?php $_SESSION['form']->captcha_input('Incorrect Validation code filled in.', 'captcha'); ?>        </td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><Br />
        <input type="submit" name="Submit" id="Submit" value="Submit" class="button" />        </td>
        </tr>
    </table>
    </form>
</div>

<img src="images/footer.jpg" alt="Roamers Rest | Natures Bed on the Soil of Africa">

<div id="footer">
    <div id="social">
        <a href="https://www.facebook.com/pages/Loerie-Guesthouse/875125949214672?ref=hl" target="_blank"><img src="images/social/facebook.png" width="32" height="32" alt="Facebook" title="Facebook" /></a>
        <a href="https://twitter.com/loerieguesth" target="_blank"><img src="images/social/twitter.png" width="32" height="32" alt="Twitter" title="Twitter" /></a>
    </div>
<a href="index.html" target="_self">Loerie Guesthouse</a> | <a href="accommodation.html" target="_self">Accommodation</a> | <a href="rates.html">Rooms & Rates</a> | <a href="activities.html" target="_self">Activities</a> | <a href="transfers.html" target="_self">Transfers</a> | <a href="contact.php" target="_self">Contact us</a><br />
All Rights Reserved Loerie Guesthouse | Designed by <a href="http://www.www.sighting-design.co.za" target="_blank">Sightings Design</a><br />
<a href="terms.html" target="_self">Terms and Conditions</a> | <a href="travel-tips.html" target="_self">Travel Tips</a>
</div></div>
</body>
</html>
