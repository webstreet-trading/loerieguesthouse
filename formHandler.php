<?php

session_start();

require_once("phpmailer/class.phpmailer.php");

class formHandler {
	var $elements = array();
	var $captcha_image = false;
	var $captcha_input = false;
	var $captcha_text = '';
	var $values = '';
	var $error_msg = array();
	
	function formHandler($values) {
		$this->values = $values;
	}
	
	function beginForm() {
		$this->elements = array();
		$this->error_msg = array();
	}

	function clearForm() {
		$this->elements = array();
		$this->error_msg = array();
	}
	
	function captcha_image($class = '') {
		$this->captcha_image = true;
		echo '<img src="captcha/captcha.php?.png" alt="CAPTCHA" class="' . $element['class'] . '" />';
	}
	
	function captcha_input($text = '', $class = '') {
		$this->captcha_input = true;
		$this->captcha_text = $text;
		echo '<input type="text" name="captchastring" class="' . $element['class'] . '" />';
	}
	
	function input_select($element) {
		if (!array_key_exists($element['name'], $this->elements)) {
			$this->elements[$element['name']] = $element;
			$default = $this->get_default($element['name'], $element['default']);
			$select = '';
			$select .= '<select name="' . $element['name'];
			if ($element['extra'] == 'multiple' && is_array($default)) { 
				$select .= '[]" multiple="multiple'; 
			}
			$select .= '" />';
			$selected_options = array();
			if ($element['extra'] == 'multiple') {
				if (is_array($default)) {
					foreach ($default as $val) {
						$selected_options[$val] = true;
					}
				}
			} else {
				$selected_options[$default] = true;
			}
			foreach ($element['value'] as $option => $label) {
				$select .= '<option value="' . $option . '"';
				if ($selected_options[$option]) {
					$select .= ' selected="selected"';
				}
				$select .= '>' . $label . "</option>";
			}
			$select .= '</select>';
			echo $select;
		}
	}

	function input_radiocheck($element) {
		$key_exists = array_key_exists($element['name'], $this->elements);
		if (!$key_exists || $element['type'] == 'radio') {
			if (!$key_exists) {
				$this->elements[$element['name']] = $element;
			}
			$exclude = true;
			$default = $this->get_default($element['name'], $element['default'], $exclude);
			$radiocheck = '';
			$radiocheck .= '<input type="'. $element['type'] . '" name="' . $element['name'] . '" value="' . $element['value'] . '" ';
			if ($element['value'] == $default) {
				$radiocheck .= 'checked="checked" ';
			}
			$radiocheck .= ' />';
			echo $radiocheck;
		}
	}

	function input_text($element) {
		if (!array_key_exists($element['name'], $this->elements)) {
			$this->elements[$element['name']] = $element;
			$default = $this->get_default($element['name'], $element['default']);
			echo '<input type="text" name="' . $element['name'] .'" class="' . $element['class'] . '" value="' . $default . '" />';
		}
	}

	function input_password($element) {
		if (!array_key_exists($element['name'], $this->elements)) {
			$this->elements[$element['name']] = $element;
			$default = $this->get_default($element['name'], $element['default']);
			echo '<input type="password" name="' . $element['name'] .'" class="' . $element['class'] . '" value="' . $default . '" />';
		}
	}

	function input_textarea($element) {
		if (!array_key_exists($element['name'], $this->elements)) {
			$this->elements[$element['name']] = $element;
			$default = $this->get_default($element['name'], $element['default']);
			echo '<textarea name="' . $element['name'] . '" class="' . $element['class'] . '">' . $default . '</textarea>';
		}
	}
	
	function get_default($name, $default, $exclude = '') {
		if (isset($this->values[$name]) || ($exclude && !empty($this->values))) {
			return $this->values[$name];
		}
		else {
			return $default;
		}
	}
	
	function validate() {
		$errors = '';
		foreach ($this->elements as $row => $element) {
			switch ($element['type']) {
				case "email":
				if (!$this->validate_email($element['text'], $this->values[$element['name']], $element['required'])) {
					$errors = 1;
				}
				break;
				case "number":
				if (!$this->validate_number($element['text'], $this->values[$element['name']], $element['length'], $element['required'])) {
					$errors = 1;
				}
				break;
				case "text":
				if (!$this->validate_text($element['text'], $this->values[$element['name']], $element['length'], $element['required'])) {
					$errors = 1;
				}
				break;
				case "checkbox":
				case "radio":
				if (!$this->validate_radiocheck($element['text'], $this->values[$element['name']], $element['name'], $element['required'])) {
					$errors = 1;
				}
			}
		}
		if ($this->captcha_image && $this->captcha_input) {
			if ($_SESSION['CAPTCHAString'] != $this->values['captchastring']) {
				$this->error_msg[] = $this->captcha_text;
				$errors = 1;
			}
		}
		if (!$errors) {
			return true;
		} else {
			return false;
		}
	}
	
	function validate_email($field_text, $email, $req = "y") {
		if ($email == "") {
			if ($req == "y") {
				$this->error_msg[] = $this->get_error(0, $field_text);
				return false;
			} else {
				return true;
			}
		} else {
			if (preg_match("/^[0-9a-z]+(([\.\-_])[0-9a-z]+)*@[0-9a-z]+(([\.\-])[0-9a-z-]+)*\.[a-z]{2,4}$/i", $email)) {
				return true;
			} else {
				$this->error_msg[] = $this->get_error(1, $field_text);
				return false;
			}
		}
	}
	
	function validate_number($field_text, $number, $length, $req = "y") {
		if ($number == "") {
			if ($req == "y") {
				$this->error_msg[] = $this->get_error(0, $field_text);
				return false;
			} else {
				return true;
			}
		} else {
			$pattern = "/^\-?[0-9]*$/";
			if (preg_match($pattern, $number)) {
				if (strlen($number) == $length || $length == 0) {
					return true;
				}
				else {
					$this->error_msg[] = $this->get_error(5, $field_text, $length);
					return false;
				}
			} else {
				$this->error_msg[] = $this->get_error(2, $field_text);
				return false;
			}
		}
	}
	
	function validate_text($field_text, $text, $length, $req = "y") {
		if (empty($text)) {
			if ($req == "y") {
				$this->error_msg[] = $this->get_error(0, $field_text);
				return false;
			} else {
				return true;
			}
		} else {
			if ($length > 0) {
				if (strlen($text) != $length) {
					$this->error_msg[] = $this->get_error(3, $field_text, $length);
					return false;
				} else {
					return true;
				}
			} else {
				return true;
			}
		}
	}
	
	function validate_radiocheck($field_text, $value, $name, $req = "y") {
		if ($req == "y") {
			if (empty($this->values[$name])) {
				$this->error_msg[] = $this->get_error(4, $field_text);
				return false;
			} else {
				if (!empty($value)) {
					if ($value != $this->values[$name]) {
						$this->error_msg[] = $this->get_error(4, $field_text);
						return false;
					} else {
						return true;
					}
				} else {
					return true;
				}
			}
		}
		else {
			return true;
		}
	}
	
	function get_error($code, $field_text, $length = '') {
		$msg = array();
		$msg[0] = 'The field "' . $field_text . '" is empty.';
		$msg[1] = 'The e-mail address in field "' . $field_text . '" is not valid.';
		$msg[2] = 'The value in field "' . $field_text . '" is not valid. It should be numeric.';
		$msg[3] = 'The text in field "' . $field_text . '" should be ' . $length . ' characters.';
		$msg[4] = 'You must select "' . $field_text . '".';
		$msg[5] = 'The value in field "' . $field_text . '" should be ' . $length . ' characters.';
		
		return $msg[$code];
	}
	
	function get_error_msg() {
		$msg = "<b>You have made the following errors:</b><br />\n";
		foreach ($this->error_msg as $error) {
			$msg .= $error . "<br />\n";
		}
		return $msg;
	}
	
	function emailForm($host, $from, $fromName, $addresses, $subject, $form, $redirect = '') {
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->Host = $host;
		$mail->From = $from;
		$mail->FromName = $fromName;
		if (is_array($addresses)) {
			foreach ($addresses as $k => $email)
				$mail->AddAddress($email);
		}
		else {
			$mail->AddAddress($addresses);
		}
		$date = date ("l, F jS, Y"); 
		$time = date ("h:i A"); 
		$msg = "Below is the result of your {$form} form. It was submitted on {$date} at {$time}.\n\n";
		foreach ($this->elements as $row => $element) {
			if (is_array($this->values[$element['name']])) {
				$msg .= "{$element['text']}:\n\n";
				foreach ($this->values[$element['name']] as $k => $value) {
					$msg .= "- {$value}\n\n";
				}
			}
			elseif ($this->values[$element['name']] != '') {
				$msg .= "{$element['text']}: {$this->values[$element['name']]}\n\n";
			}
		}
		$mail->Subject = $subject;
		$mail->Body = $msg;
		$mail->WordWrap = 50;
		$mail->Send();
		
		header ("Location: {$redirect}"); 
	}
	
	function getElements() {
		$elem = array();
		foreach ($this->elements as $row => $element) {
			$elem[$element['name']] = $this->values[$element['name']];
		}
		return $elem;
	}
	
	function DESTROY() {
      settype($this, 'null');
   }
}

?>