/*** 
    Simple jQuery Slideshow Script
    Released by Jon Raasch (jonraasch.com) under FreeBSD license: free to use or modify, not responsible for anything, etc.  Please link out to me if you like it :)
***/

function slideSwitch() {
    jQuery.noConflict();
    var $active = jQuery('#slideshow IMG.active');

    if ( $active.length == 0 ) $active = jQuery('#slideshow IMG:last');

    // use this to pull the images in the order they appear in the markup
    var $next =  $active.next().length ? $active.next()
        : jQuery('#slideshow IMG:first');

    // uncomment the 3 lines below to pull the images in random order
    
    // var $sibs  = $active.siblings();
    // var rndNum = Math.floor(Math.random() * $sibs.length );
    // var $next  = $( $sibs[ rndNum ] );


    $active.addClass('last-active');

    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 0.0, function() {
            $active.removeClass('active last-active');
        });
}

// var img = document.querySelectorAll('.img-slide');

// console.log(img);

jQuery(function() {
    setInterval( "slideSwitch()", 2500 );
});

var imgs = Array.prototype.slice.call(document.querySelectorAll('.img-slide'));

imgs.map(function (img) {
    console.log(img);
});

// console.log(imgs);